import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    input: {
        background: '#f4f4f4',
        borderBottomWidth: 0,
        padding: '0 5px'
    },
}));

export default useStyles