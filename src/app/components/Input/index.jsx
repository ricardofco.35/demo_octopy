import React from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import useStyles from './styles';

const Input = ({ ...props }) => {
	const classes = useStyles();
	return (
		<TextField
			{...props}
			className={classes.input}
			InputProps={{ disableUnderline: true }}
		/>
	);
};

Input.propTypes = {
	fullWidth: PropTypes.bool.isRequired,
};

Input.defaultProps = {
	fullWidth: true,
};

export default Input;
