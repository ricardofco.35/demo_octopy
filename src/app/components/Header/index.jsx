import { Grid } from '@material-ui/core';
import React from 'react';
import MenuIcon from '@material-ui/icons/Menu';
import { useStyles, styles } from './styles';

const Header = () => {
	const classes = useStyles();

	return (
		<div className={classes.root}>
			<Grid
				container
				wrap='nowrap'
				alignItems='center'
				justifyContent='space-between'>
				<Grid item>
					<img style={styles.img} src='img/logo.png' alt='' />
				</Grid>
				<Grid item>
					<MenuIcon style={{ color: '#fff' }} />
				</Grid>
			</Grid>
		</div>
	);
};

export default Header;
