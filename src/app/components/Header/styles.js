import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme) => ({
    root: {
        flex: 1,
        marginBottom: theme.spacing(2)
    },
}));

export const styles = {
    img: {
        width: '5rem',
        heigth: '5rem'
    }
}