import React from 'react';
import {
	IconButton,
	Table,
	TableHead,
	TableBody,
	TableRow,
	TableCell,
	TablePagination,
	Typography,
} from '@material-ui/core';
import Paper from '../Paper';
import { useStyles } from './styles';
import { tablesStrings } from '../../constants';
import Loading from '../Loading';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import AddIcon from '@material-ui/icons/Add';

const PaginationTable = ({ title, columns, rows, loading, ...props }) => {
	const classes = useStyles();
	const [rowsPerPage, setRowsPerPage] = React.useState(5);
	const [page, setPage] = React.useState(0);

	const handleChangePage = (event, newPage) => {
		setPage(newPage);
	};

	const handleChangeRowsPerPage = event => {
		setRowsPerPage(+event.target.value);
	};

	const renderHeader = () => {
		return columns.map((item, index) => (
			<TableCell key={index} className={classes.tableCell}>
				{item.title}
			</TableCell>
		));
	};

	const renderInventary = quantity => {
		console.log(quantity);
		let status = 'En stock',
			color = 'green';
		if (quantity === 0 || quantity === '0') {
			status = 'Agotado';
			color = 'red';
		} else if (quantity > 0 && quantity <= 20) {
			status = 'Limitado';
			color = 'orange';
		}
		return (
			<TableCell style={{ color }}>
				<div
					style={{
						width: '10px',
						height: '10px',
						backgroundColor: color,
						color: 'transparent',
						borderRadius: '50%',
						display: 'inline-block',
						marginRight: '5px',
					}}></div>
				{status}
			</TableCell>
		);
	};

	if (loading) return <Loading />;

	if (rows?.length < 1)
		return (
			<Typography variant='subtitle1'>
				{props.emptyText || tablesStrings.EMPTY_TABLE}
			</Typography>
		);

	return (
		<Paper style={{ overflowX: 'auto' }}>
			<Typography variant='h5' gutterBottom>
				{title || 'Tu titulo'}
			</Typography>
			<IconButton onClick={props.handleAdd} className={classes.addButton}>
				<AddIcon style={{ color: '#000' }} />
			</IconButton>
			<Table
				style={{
					width: '100%',
					overflowX: 'auto',
				}}>
				<TableHead className={classes.tableHead}>
					<TableRow>{renderHeader()}</TableRow>
				</TableHead>
				<TableBody>
					{rows
						.slice(
							page * rowsPerPage,
							page * rowsPerPage + rowsPerPage
						)
						.map((item, index) => (
							<TableRow key={index}>
								<TableCell align='left'>{item.name}</TableCell>
								<TableCell align='left'>
									{item.category}
								</TableCell>
								<TableCell align='left'>{item.price}</TableCell>
								<TableCell>{item.quantity}</TableCell>
								{renderInventary(item.quantity)}
								<TableCell className='px-0'>
									<IconButton
										onClick={() => props.handleEdit(item)}>
										<EditIcon style={{ color: '#000' }} />
									</IconButton>
									<IconButton
										onClick={() =>
											props.handleDelete(item)
										}>
										<DeleteIcon style={{ color: '#000' }} />
									</IconButton>
								</TableCell>
							</TableRow>
						))}
				</TableBody>
			</Table>

			<TablePagination
				// style={{ display: 'block', width: '100%'}}
				rowsPerPageOptions={[5, 10, 25]}
				component='div'
				count={rows.length}
				rowsPerPage={rowsPerPage}
				page={page}
				backIconButtonProps={{
					'aria-label': tablesStrings.PREVIOUS_PAGE,
				}}
				nextIconButtonProps={{
					'aria-label': tablesStrings.NEXT_PAGE,
				}}
				onPageChange={handleChangePage}
				onRowsPerPageChange={handleChangeRowsPerPage}
			/>
		</Paper>
	);
};

export default PaginationTable;
