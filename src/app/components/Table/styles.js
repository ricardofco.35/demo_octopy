import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme) => ({
    tableHead: {
        background: "#f2f2f2",
    },
    tableCell: {
        fontWeight: 600
    },
    addButton: {
        background: '#61F25B',
        marginLeft: 'auto',
        display: 'block'
    }
}));
