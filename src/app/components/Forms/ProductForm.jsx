import React, { useEffect, useState } from 'react';
import { useFormik } from 'formik';
import { productsStrings, validations } from '../../constants';
import { useSelector, useDispatch } from 'react-redux';
import {
	addProductAction,
	editProductAction,
} from '../../state/products/actions';
import { v4 as uuidv4 } from 'uuid';
import useStyles from './styles';

import Input from '../Input';
import { Button, Grid, Typography } from '@material-ui/core';
import ImageUpload from '../ImageUpload';

const CoursesForm = ({ title, isEdition, initialData, ...props }) => {
	const classes = useStyles();
	const dispatch = useDispatch();
	const [images, setImage] = useState([]);
	const { addLoading } = useSelector(state => state.products);

	const addProduct = course => dispatch(addProductAction(course));
	const editProduct = course => dispatch(editProductAction(course));

	const addImage = image => {
		setImage(prevState => [...prevState, image]);
	};

	useEffect(() => {
		if (initialData) {
			setImage(initialData.images);
		}
	}, [initialData]);

	const initialValues = initialData
		? initialData
		: {
				name: '',
				description: '',
				category: '',
				quantity: 0,
				price: 0,
				color: '',
				marca: '',
				composicion: '',
				target: '',
		  };

	const formik = useFormik({
		initialValues: initialValues,
		validationSchema: validations.validationSchema,
		onSubmit: values => {
			values.images = images;
			if (!isEdition) {
				values.id = uuidv4();
				addProduct(values);
			} else {
				editProduct(values);
			}
		},
	});
	return (
		<form onSubmit={formik.handleSubmit} style={{ padding: '1.875rem' }}>
			<Grid container spacing={3} justifyContent='center'>
				<Grid item sm={4} xs={12}>
					<Grid container spacing={3}>
						<Grid item sm={12}>
							<ImageUpload
								images={images}
								onChange={file => {
									addImage(file);
								}}
							/>
						</Grid>
						<Grid item sm={12}>
							<Typography variant='h6'>
								{productsStrings.DESCRIPTION_TITLE}
							</Typography>
						</Grid>
						<Grid item sm={12}>
							<Input
								name='name'
								label='Nombre'
								value={formik.values.name}
								onChange={formik.handleChange('name')}
								error={
									formik.touched.name &&
									Boolean(formik.errors.name)
								}
								helperText={
									formik.touched.name && formik.errors.name
								}
							/>
						</Grid>
						<Grid item sm={12}>
							<Input
								name='description'
								multiline
								minRows={5}
								label='Descripción'
								value={formik.values.description}
								onChange={formik.handleChange('description')}
								error={
									formik.touched.description &&
									Boolean(formik.errors.description)
								}
								helperText={
									formik.touched.description &&
									formik.errors.description
								}
							/>
						</Grid>
					</Grid>
				</Grid>
				<Grid item sm={8} xs={12}>
					<Grid container spacing={3}>
						<Grid item xs={12}>
							<Typography variant='h6' gutterBottom>
								{productsStrings.GENERAL_TITLE}
							</Typography>
						</Grid>

						<Grid item sm={6} xs={12}>
							<Input
								name='category'
								label='Categoria'
								value={formik.values.category}
								onChange={formik.handleChange('category')}
								error={
									formik.touched.category &&
									Boolean(formik.errors.category)
								}
								helperText={
									formik.touched.category &&
									formik.errors.category
								}
							/>
						</Grid>
						<Grid item sm={6} xs={12}>
							<Input
								name='quantity'
								label='Stock'
								value={formik.values.quantity}
								onChange={formik.handleChange('quantity')}
								error={
									formik.touched.quantity &&
									Boolean(formik.errors.quantity)
								}
								helperText={
									formik.touched.quantity &&
									formik.errors.quantity
								}
							/>
						</Grid>
						<Grid item sm={6} xs={12}>
							<Input
								name='price'
								label='Precio'
								value={formik.values.price}
								onChange={formik.handleChange('price')}
								error={
									formik.touched.price &&
									Boolean(formik.errors.price)
								}
								helperText={
									formik.touched.price && formik.errors.price
								}
							/>
						</Grid>
					</Grid>

					<Grid container spacing={3} className={classes.marginTop}>
						<Grid item xs={12}>
							<Typography variant='h6'>
								{productsStrings.ABOUT_PRODUCT}
							</Typography>
						</Grid>
						<Grid item sm={6} xs={12}>
							<Input
								name='color'
								label='Color'
								value={formik.values.color}
								onChange={formik.handleChange('color')}
								error={
									formik.touched.color &&
									Boolean(formik.errors.color)
								}
								helperText={
									formik.touched.color && formik.errors.color
								}
							/>
						</Grid>
						<Grid item sm={6} xs={12}>
							<Input
								name='marca'
								label='Marca'
								value={formik.values.marca}
								onChange={formik.handleChange('marca')}
								error={
									formik.touched.marca &&
									Boolean(formik.errors.marca)
								}
								helperText={
									formik.touched.marca && formik.errors.marca
								}
							/>
						</Grid>
						<Grid item sm={6} xs={12}>
							<Input
								name='composicion'
								label='Composición'
								value={formik.values.composicion}
								onChange={formik.handleChange('composicion')}
								error={
									formik.touched.composicion &&
									Boolean(formik.errors.composicion)
								}
								helperText={
									formik.touched.composicion &&
									formik.errors.composicion
								}
							/>
						</Grid>
						<Grid item sm={6} xs={12}>
							<Input
								name='target'
								label='Tarjet'
								value={formik.values.target}
								onChange={formik.handleChange('target')}
								error={
									formik.touched.target &&
									Boolean(formik.errors.target)
								}
								helperText={
									formik.touched.target &&
									formik.errors.target
								}
							/>
						</Grid>
					</Grid>
					<Grid container justifyContent='flex-end'>
						<Button type='submit' disabled={addLoading}>
							Guardar
						</Button>
					</Grid>
				</Grid>
			</Grid>
		</form>
	);
};

export default CoursesForm;
