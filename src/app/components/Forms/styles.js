import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    marginTop: {
        marginTop: '50px',
        marginBottom: '20px'
    },
}));
export default useStyles;