const styles = {
    container: {
        borderRadius: 0,
        position: 'relative',
        height: '200px',
        padding: 0
    },
    button: {
        background: '#61F25B',
        borderRadius: '50%',
        width: '30px',
        height: '30px',
        padding: '10px',
        fontSize: '20px',
        fontWeight: 'bold',
        cursor: 'pointer',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonContainer: {
        position: 'absolute',
        bottom: 10,
        right: 0,
        left: 0,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }
};

export default styles;