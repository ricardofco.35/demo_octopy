import React from 'react';
import Paper from '../Paper';
import styles from './styles';
import Carrusel from '../Carrusel';
import { convertBase64 } from '../../utils';

const ImageUpload = (props) => {
	const handleChange = async e => {
		const file = e.target.files[0];
		const base64 = await convertBase64(file);
		props.onChange(base64);
	};
	return (
		<Paper style={styles.container} square>
			<Carrusel images={props.images} />
			<div style={styles.buttonContainer}>
				<label htmlFor='file-upload' style={styles.button}>
					+
				</label>
				<input id='file-upload' name='file-upload' type='file' onChange={handleChange} />
			</div>
		</Paper>
	);
};

export default ImageUpload;
