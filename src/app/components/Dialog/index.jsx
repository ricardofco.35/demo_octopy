import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';


const ResponsiveDialog = ({ isopen, onClose, ...props }) => {
	const theme = useTheme();
	const fullScreen = useMediaQuery(theme.breakpoints.down('xs'));
	return (
		<Dialog
			fullScreen={fullScreen}
			open={isopen}
			fullWidth={true}
			onClose={onClose}
			maxWidth='md'	
			aria-labelledby='responsive-dialog-title'>
			{props.children}
		</Dialog>
	);
};

export default ResponsiveDialog;
