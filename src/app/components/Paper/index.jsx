import React from 'react';
import Paper from '@material-ui/core/Paper';
import useStyles from './styles';

export default function SimplePaper({ children, ...props }) {
	const classes = useStyles();
	return (
		<Paper
			elevation={3}
			className={`${classes.root} ${props.className}`}
			{...props}>
			{children}
		</Paper>
	);
}
