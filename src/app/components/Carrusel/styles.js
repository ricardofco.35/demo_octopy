import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme) => ({
    button: {
        height: '50px',
        width: '10px',
        minWidth: '10px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        background: 'transparent',
        position: 'absolute',
        cursor: 'pointer',
        zIndex: 100,
    },
    goForward: {
        right: 0
    },
}));

export const styles = {
    mainContainer: {
        display: 'flex',
        position: 'relative',
        width: '100%',
        height: '200px',
        alignItems: 'center',
        background: '#f4f4f4'
    },
    imageContainer: {
        display: 'flex',
        overflowX: 'hidden',
        width: '100%',
        aligIntems: 'center'
    },
    icon: {
        fontSize: '40px',
        color: '#fff',
    },
    image: {
        objectFit: 'contain',
        width: '100%',
        height: '200px',
        minWidth: '100%',
        transition: 'transform .5s'
    }
}

export default styles;