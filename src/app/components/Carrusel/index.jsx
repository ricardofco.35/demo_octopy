import React, { useState } from 'react';
import Control from './control';
import { styles } from './styles';
import CameraAltIcon from '@material-ui/icons/CameraAlt';

const Carrusel = ({ images }) => {
	const [position, setPosition] = useState(0);

	const goBack = () => {
		if (position > 0) setPosition(position - 1);
	};

	const goForward = () => {
		if (position < images.length - 1) {
			setPosition(position + 1);
		}
	};
	return (
		<div style={styles.mainContainer}>
			<Control type='back' onClick={goBack} />
			<div style={styles.imageContainer}>
				{images && images.length > 0 ? (
					images.map((image, index) => (
						<img
							position={position}
							key={index}
							src={image}
							alt={index}
							style={{
								transform: `translateX(-${position}00%)`,
								...styles.image,
							}}
						/>
					))
				) : (
					<CameraAltIcon
						fontSize='large'
						style={{ margin: 'auto' }}
					/>
				)}
			</div>
			<Control type='forward' onClick={goForward} />
		</div>
	);
};

export default Carrusel;
