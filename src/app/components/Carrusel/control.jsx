import { IconButton } from '@material-ui/core';
import React from 'react';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import { useStyles } from './styles';

const Control = ({ onClick, ...props }) => {
	const classes = useStyles();
	return (
		<IconButton
			onClick={onClick}
			className={`${classes.button} ${
				props.type === 'forward' ? classes.goForward : null
			}`}>
			{props.type === 'forward' ? (
				<ArrowForwardIosIcon style={{ color: '#000' }} />
			) : (
				<ArrowBackIosIcon style={{ color: '#000' }} />
			)}
		</IconButton>
	);
};

export default Control;
