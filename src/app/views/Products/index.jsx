import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
	deleteProductAction,
	getProductEditAction,
	getProductsAction,
} from '../../state/products/actions';

import { Grid } from '@material-ui/core';
import { Table, Dialog, ProductForm, Alert } from '../../components';
import { productsStrings } from '../../constants';

const Products = () => {
	const dispatch = useDispatch();
	const [open, setOpen] = useState(false);
	const [isEdition, setEdition] = useState(false);
	const { products, error, loading, productEdit, edited } = useSelector(
		state => state.products
	);

	useEffect(() => {
		const getProducts = () => dispatch(getProductsAction());
		getProducts();
	// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	useEffect(() => {
		if (edited) {
			handleClose();
		}
	// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [edited]);

	const deleteProduct = product => dispatch(deleteProductAction(product.id));

	const editProduct = product => {
		dispatch(getProductEditAction(product));
		setEdition(true);
		handleClickOpen();
	};

	const handleClickOpen = () => {
		setOpen(true);
	};

	const handleClose = () => {
		setOpen(false);
		setEdition(false);
		if (productEdit) dispatch(getProductEditAction(null));
	};

	return (
		<>
			<Grid item sx={11}>
				<Table
					loading={loading}
					title={productsStrings.TABLE_PRODUCTS_TITLE}
					columns={productsStrings.TABLE_COLUMNS}
					rows={products}
					handleDelete={deleteProduct}
					handleEdit={editProduct}
					handleAdd={handleClickOpen}
				/>
			</Grid>
			<Dialog isopen={open} onClose={handleClose}>
				<ProductForm
					initialData={isEdition ? productEdit : null}
					title={
						isEdition
							? productsStrings.EDIT_PRODUCT_TITLE
							: productsStrings.ADD_PRODUCT_TITLE
					}
					secondAction={handleClose}
					isEdition={isEdition}
				/>
			</Dialog>
			{error && (
				<Alert isopen={true} message={productsStrings.SERVER_ERROR} />
			)}
		</>
	);
};

export default Products;
