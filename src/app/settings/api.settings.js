import { Enviroment } from './environment';

class ApiSettings {
	constructor() {
		this.API_URL = Enviroment.apiUrl;
		this.ENDPOINT_PRODUCTS = this.API_URL + Enviroment.endpointUnauthorized.products;
	}
}

export default new ApiSettings();
