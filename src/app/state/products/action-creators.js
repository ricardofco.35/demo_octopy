import * as types from './types';

export const getProducts = () => ({
	type: types.PRODUCTS_PENDING,
	payload: true,
});

export const setProducts = data => ({
	type: types.PRODUCTS_FULFILLED,
	payload: data,
});

export const getProductsRejected = () => ({
	type: types.PRODUCTS_REJECTED,
	payload: true,
});

export const addProduct = () => ({
	type: types.ADD_PRODUCT_PENDING,
});

export const addProductFulfilled = data => ({
	type: types.ADD_PRODUCT_FULFILLED,
	payload: data,
});

export const addProductError = () => ({
	type: types.ADD_PRODUCT_REJECTED,
});

export const getProductEdit = product => ({
	type: types.STORE_CURRENT_PRODUCT,
	payload: product,
});

export const editProduct = () => ({
	type: types.EDIT_PRODUCT_PENDING,
});

export const editProductFulfilled = data => ({
	type: types.EDIT_PRODUCT_FULFILLED,
	payload: data,
});

export const editProductError = () => ({
	type: types.EDIT_PRODUCT_REJECTED,
});

export const getProductDetail = () => ({
	type: types.PRODUCT_DETAIL_PENDING,
	payload: true,
});

export const setProductDetail = data => ({
	type: types.PRODUCT_DETAIL_FULFILLED,
	payload: data,
});

export const getProductDetailRejected = () => ({
	type: types.PRODUCT_DETAIL_REJECTED,
	payload: true,
});

export const deleteProduct = () => ({
	type: types.DELETE_PRODUCT_PENDING,
});

export const deleteProductFulfilled = data => ({
	type: types.DELETE_PRODUCT_FULFILLED,
	payload: data,
});

export const deleteProductError = () => ({
	type: types.DELETE_PRODUCT_REJECTED,
});