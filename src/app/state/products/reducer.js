import * as types from './types';

const initialState = {
	products: [],
	error: false,
	loading: false,
	addLoading: false,
	edited: false,
	productEdit: null,
};

export default function reducer(state = initialState, action) {
	switch (action.type) {
		case types.PRODUCTS_PENDING:
		case types.PRODUCT_DETAIL_PENDING:
			return {
				...state,
				loading: action.payload,
			};
		case types.PRODUCTS_FULFILLED:
			return {
				...state,
				loading: false,
				error: false,
				products: action.payload,
			};
		case types.PRODUCTS_REJECTED:
		case types.PRODUCT_DETAIL_REJECTED:
			return {
				...state,
				loading: false,
				error: true,
			};
		case types.ADD_PRODUCT_PENDING:
		case types.EDIT_PRODUCT_PENDING:
			return {
				...state,
				addLoading: true,
				edited: false,
			};
		case types.ADD_PRODUCT_FULFILLED:
			return {
				...state,
				addLoading: false,
				edited: true,
				products: [...state.products, action.payload],
			};
		case types.ADD_PRODUCT_REJECTED:
		case types.EDIT_PRODUCT_REJECTED:
			return {
				...state,
				addLoading: false,
				error: true,
				edited: false,
			};
		case types.EDIT_PRODUCT_FULFILLED:
			return {
				...state,
				addLoading: false,
				error: false,
				edited: true,
				products: state.products.map(c =>
					c.id === action.payload.id ? (c = action.payload) : c
				),
			};
		case types.STORE_CURRENT_PRODUCT:
			return {
				...state,
				edited: false,
				productEdit: action.payload,
			};
		case types.DELETE_PRODUCT_FULFILLED:
			return {
				...state,
				products: state.products.filter(c => c.id !== action.payload),
			};

		case types.PRODUCT_DETAIL_FULFILLED:
			return {
				...state,
				error: false,
				productEdit: action.payload,
				loading: false,
			};

		default:
			return state;
	}
}
