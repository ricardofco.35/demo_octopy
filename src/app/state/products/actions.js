import axios from 'axios';
import * as actionCreators from './action-creators';
import { apiSettings } from '../../settings';

export function getProductsAction() {
	return async dispatch => {
		dispatch(actionCreators.getProducts());
		try {
			const response = await axios.get(apiSettings.ENDPOINT_PRODUCTS);
			dispatch(actionCreators.setProducts(response.data));
		} catch (error) {
			dispatch(actionCreators.getProductsRejected());
		}
	};
}

export function getProductDetailAction(id) {
	return async dispatch => {
		dispatch(actionCreators.getProductDetail());
		try {
			const response = await axios.get(
				`${apiSettings.ENDPOINT_PRODUCTS}?id=${id}`
			);
			dispatch(actionCreators.setProductDetail(response.data[0]));
		} catch (error) {
			dispatch(actionCreators.getProductDetailRejected());
		}
	};
}

export function addProductAction(product) {
	return async dispatch => {
		dispatch(actionCreators.addProduct());
		try {
			await axios.post(apiSettings.ENDPOINT_PRODUCTS, product);
			dispatch(actionCreators.addProductFulfilled(product));
		} catch (error) {
			dispatch(actionCreators.addProductError(true));
		}
	};
}

export function editProductAction(product) {
	return async dispatch => {
		dispatch(actionCreators.editProduct());
		try {
			await axios.put(
				`${apiSettings.ENDPOINT_PRODUCTS}/${product.id}`,
				product
			);
			dispatch(actionCreators.editProductFulfilled(product));
		} catch (error) {
			dispatch(actionCreators.editProductError(true));
		}
	};
}

export function deleteProductAction(id) {
	return async dispatch => {
		dispatch(actionCreators.deleteProduct());
		try {
			await axios.delete(`${apiSettings.ENDPOINT_PRODUCTS}/${id}`);
			dispatch(actionCreators.deleteProductFulfilled(id));
		} catch (error) {
			dispatch(actionCreators.deleteProductError(true));
		}
	};
}

export function getProductEditAction(product) {
	return dispatch => {
		dispatch(actionCreators.getProductEdit(product));
	};
}
