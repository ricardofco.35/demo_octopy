export const TABLE_PRODUCTS_TITLE = 'Mis productos';

export const EDIT_PRODUCT_TITLE = 'Editar producto';
export const ADD_PRODUCT_TITLE = 'Agrega un nuevo producto'
export const DESCRIPTION_TITLE = 'DESCRIPCIÓN';
export const GENERAL_TITLE = 'GENERAL';
export const ABOUT_PRODUCT = 'ACERCA DEL PRODUCTO';
export const SERVER_ERROR = 'No se obtuvieron los productos, verifica que el servidor json este corriendo';
export const TABLE_COLUMNS = [
    { title: 'Nombre' },
    { title: 'Categoría' },
    { title: 'Precio' },
    { title: 'Cantidad' },
    { title: 'Inventario' },
    { title: 'Acción' },
];