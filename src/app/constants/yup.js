import * as yup from 'yup';

export const productName = yup
	.string('Ingresa el nombre')
	.min(3, 'El nombre debe contener al menos 3 caracteres')
	.required('nombre requerido');
export const description = yup
	.string('Ingresa una breve descripción del producto')
	.min(5, 'La descripción debe tener minimo 5 caracteres')
	.required('Descripción requerida');

export const price = yup.number().required('Ingresa la cantidad disponible').positive('Ingresa un numero mayor de cero');


export const validationSchema = yup.object({
	name: productName,
	description,
	price,
});