import * as productsStrings from './productsStrings'
import * as tablesStrings from './tablesString';
import * as validations from './yup';

export { productsStrings, tablesStrings, validations }