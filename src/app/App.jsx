import { Container } from '@material-ui/core';
import React from 'react';
import { Header } from './components';
import Products from './views/Products';
import { makeStyles } from '@material-ui/core/styles';
import { Provider } from 'react-redux';
import store from './state/store';

const useStyles = makeStyles(theme => ({
	root: {
		padding: theme.spacing(2),
	},
}));

function App() {
	const classes = useStyles();
	return (
		<Provider store={store}>
			<Container maxWidth='lg' fixed className={classes.root}>
				<Header />
				<Products />
			</Container>
		</Provider>
	);
}

export default App;
